## This is the Knights of Columbus repository for council 7079.

This is running on Hugo hosted by gitlab pages.

```


#### Blog posts get added as Markdown files in /content/blog/ i

They are automatically displayed on 'date' listed in markdown headers.
Header below.

+++
title = "Example Blog Post"
date = "2015-10-02"
tags = ["programming", "project", "hugo"]
categories = ["events"]
banner = "img/posts/pictre.jpg"
author = "John Doe"
+++
```


#### Main config file is config.toml

Theme was ported to hugo by [DevCows](https://github.com/devcows/hugo-universal-theme).

