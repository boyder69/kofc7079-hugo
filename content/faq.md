+++
title = "About our Council"
description = "About KofC 7079"
+++

### OLBH Council 7079 founded on February 27th, 2006.

Our Lady of the Black Hills is home to KoC Council 7079 with over 100 active council members.  
We live united in **faith** and **charity**, supporting our diocese, parish, and communitiy.  
[Join Us](https://kofc7079.org/contact/) in brotherhood.  

#### Current Officers
|                         |                         |                         |
| ----------------------- | ----------------------- | ----------------------- |
| <img width=300/>        | <img width=300/>        | <img width=300/>        |
| **Grand Knight**        | **Deputy Grand Knight** | **Inside Gaurd**        |
| Aaron Denekamp          | Wade Rhodes             | Mike Brown              |
| **Chaplain**            | **Lecturer**            | **Outside Gaurd**       |
| Fr. Andrzej Wyrostek    | Wes Mendenhall          | Brady Rothschadl        |
| **Financial Secretary** | **Chancellor**          | **1-year Trustee**      |
| Jeremy Thompson         | Jason Serfling          | Dwayne Yantes           |
| **Treasurer**           | **Advocate**            | **2-year Trustee**      |
| Tom Collings Sr.        | Andrew Boyd             | Richard Long            |
| **Warden**     	  | **Recorder**            | **3-year Trustee**      |
| Jim Landers             | Todd Curtis             | John Gehlsen            |
----


#### Knights in Action 
#### In service to one, In service to all  

**$5000+** Annually in Charitable Contributions  
**3000+** Man-Hours of Charitable Service Annually  

[Learn more about the Knights](http://kofc.org)  
