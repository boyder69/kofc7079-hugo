+++
title = "BH Pregnancy Center Walk/Jog/Run For Life"
date = "2019-05-02"
tags = ["right-to-life"]
categories = ["events","right-to-life"]
banner = "img/posts/bhpc-5k-19.PNG"
author = "Andrew Boyd"
+++

## Join the Knights on a Walk/Jog/Run for Life Saturday May 11th, 2019.
<img style="float: center;" src="/img/posts/bhpc-5k-19.PNG">   
