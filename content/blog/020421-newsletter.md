+++
title = "February 2021 Newsletter"
date = "2021-02-04"
tags = ["newsletter"]
categories = ["news"]
banner = "img/posts/newsletter/21/feb/image1.jpeg"
author = "Aaron Denekamp"
+++

# February 2021 Knight of Columbus 7079 Newsletter  


## Knight of the Month - January

![](/img/posts/newsletter/21/feb/image1.jpeg#small)

#### John Gehlsen

Knight of the month for January was John Gehlsen.

John has been a Knight since September of 2008, since joining 7079 John
has been an active member of the council. He is also a member of the
4^th^ Degree Assembly here in Rapid City.

Johnny has held various officer positions including: Deputy Grand
Knight, Grand Knight, and currently is a Trustee.

He is also the current State Online Membership Officer.

John has been involved with nearly every event or Knights function since
coming aboard. While he was DGK & GK he worked very hard on membership
growth. I don’t know the specific numbers, but John is responsible
himself for nearly doubling our council’s membership numbers.

John was raised in the Mission SD area, after graduating he attended
Mitchell Tech for HVAC. While in Mitchel John met his wife Rhonda, after
graduation they moved to Rapid City and were married in 1997.

John and Rhonda have 4 children, Paige (22) Isaac (19) Hannah (16) &
Gabriel (13)

John has owned & operated his own HVAC business for many years. One
thing about being your own boss, you have somewhat of a flexible
schedule; which has helped him be around for like I said earlier just
about every Knights function you could think of!!

Aside from the Knights and working, John enjoys spending time with his
family. His and Rhonda’s folks still live in the Winner & Mission areas.
Their family enjoys camping, Gehlsen’s have been on the Knights campout
many times with the Knights crew.

I'm trying to get him to buy a Harley, so then he can be the chair for
Knights on Bikes too!! Ha Ha, thanks Gehlsen for all you do.

![](/img/posts/newsletter/21/feb/image2.jpg#auto)  

**SAVE THE DATE!**  
* February 4th Monthly Membership meeting 7pm
* February 17th Ash Wednesday
* February 19th Stations of the Cross
* February 19th First Fish Fry
* March 4th Monthly Membership meeting

**February Birthday's**  
* Feb 1st Jeremy Reeve
* Feb 2nd Todd Curtis
* Feb 4th Dewayne Yantes
* Feb 14th Chris Potthoff
* Feb 15th Wayne Ebbers
* Feb 16th Pascal Bedard
* Feb 18th Aaron Johnson
* Feb 18th Josh Philbrick
* Feb 25th Faron Schmidt
* Feb 27th Deacon Greg Sass  

## Breakfast Burrito Fundraiser  

On January 24th we had our first breakfast burrito fundraiser.

After two weeks of pre orders we were ready to go.

Saturday night after the 5pm Mass we started cooking 60 pounds of
sausage and about 30 pounds of bacon, got everything ready to go to
start bright and early Sunday morning.

We had a nice turnout of Knights that showed up to help out! We needed a
lot of help to get these burritos assembled and wrapped.

6 am Sunday morning we started cooking eggs & assembling 250 burritos. I
am not a morning person, took a lot to get me out of bed that early on a
Sunday morning, but the end result was well worth it!

After it was all said and done, after expenses the council raised
\$922!! Not bad for a breakfast, here’s some pics

![](/img/posts/newsletter/21/feb/image3.jpeg#auto)

![](/img/posts/newsletter/21/feb/image4.jpeg#auto)  

![](/img/posts/newsletter/21/feb/image5.jpeg#r90small)  

![](/img/posts/newsletter/21/feb/image6.jpeg#auto)  

![](/img/posts/newsletter/21/feb/image7.jpg#auto)  

## Lenten Fish Fry's  

Ash Wednesday is just around the corner, this year Lent starts on the
17th of this month.

So.... The council will again put on our weekly fish fry's.

This year things will be a little different, with not being able to hold
a dinner downstairs in the dining room, we’re going to have to think
outside the box again.

We’ll be offering fish fry dinners to go. Some of us will be in the
narthex after each Mass this weekend with a sign-up sheet for to go
orders. We’re going to be doing both baked and fried cod again, sides of
cole slaw and mac & cheese.

Each to go order will get two pieces of fish, & a helping of slaw & mac
& cheese.

We’ve had good luck with free will donations, so I think we should stick
with what works!

Each week we’ll have to get orders for the following Friday’s to go
dinners, this will take a commitment from you brother Knights to help
out to make this a success.

Thanks in advance to you all!!

## Coats For Kids

At the December membership meeting we had 4 generous Knights who pitched
in \$110 each to purchase two cases of coats to children in need. The
Coats for Kids Program is a Knights Faith in Action Program where
Councils can purchase new winter coats at a discount to be distributed
to the children in need in our local communities. Thomas Collings,
Richard Long, David Allardyce and John Gehlsen kicked in the money to
purchase two cases, one case for boys and one for girls.

We’ve had the coats downstairs in the food pantry area for a few weeks
now, last I had spoken to Ned Keeler, he had mentioned that there hasn’t
been many folks that have taken coats. We’ll keep offering them to those
in need, if anyone knows of someone in need let us know.

## Neighbors Men's Group

Before the COVID hit, about 8-10 of guys from the council were meeting
at Neighbors Thursday nights at 7pm to go over each week’s upcoming
Sunday readings and participate with a set of reflection questions. Jim
Landers is there every Thursday at 7pm. Anyone that would like to join
us is welcome.

## 4th Degree Regalia Loan Program

We are pleased to announce the New Regalia Lon Program. It is our goal
to increase the number of Brother Knights that have the new regalia and
to help those who may not be able to afford it at one time. We have set
aside some funds to be used as a loan to Brother Knights for them to
purchase the new regalia. The loan amount maximum is \$500 which is
enough to purchase the outfit however the sword, sash, and gloves would
be extra. The loan will have to be repaid over 10 consecutive monthly
payments. I can get forms which must be completed and returned to be
considered. Not all applicants will be approved. Again, our goal is to
increase the number in our Honor Guard which is an important part of the
Patriotic degree.

## Dynamic Catholic is offering a Lenten online devotional

Put on by Dynamic Catholic based on Matthew Kelly’s book “I Heard God
Laugh” that we all received from OLBH at Christmas time

A Lenten devotional online book study is being offered through Dynamic
Catholic this Lenten season, check it out!!

![](/img/posts/newsletter/21/feb/image8.jpg#auto)

## Silver Rose
**Faith in Action Program**

The Silver Rose Program is now continuing as scheduled. Live-streamed
prayer services remain the recommended option for the program.

Share the message of Our Lady of Guadalupe and promote respect for life
by participating in this meaningful pilgrimage. The Silver Rose program
demonstrates the unity between Knights of Columbus in Canada, the United
States and Mexico, through a series of prayer services promoting the
dignity of all human life and honoring Our Lady.

Each year, from early March through mid-December, Silver Roses are
stewarded by Knights of Columbus councils along routes from Canada to
Mexico. Every stop the Silver Rose makes throughout the pilgrimage is a
rosary-centered occasion for Knights, parishioners and community members
to pray for respect for life, for the spiritual renewal of each nation,
and for the advancement of the message of Our Lady of Guadalupe.

Here is a couple Silver Rose Links;  
[*https://www.youtube.com/watch?v=FiKTwKw-3Y4*](https://www.youtube.com/watch?v=FiKTwKw-3Y4)  
*https://www.facebook.com/KofCAlabama/videos/1446167312253623*  

![](/img/posts/newsletter/21/feb/image9.jpg#auto)

Almighty and ever living God, we come before you as a people of faith
and ask you to bless us. In our service as Knights of Columbus, we pray
for the gift of the Holy Spirit to guide us as we bring others to Christ
through our witness. Enliven us with your Spirit so that we will develop
outstanding council programs for our parishes, communities, families,
and youth in a way that will prompt the Catholic men in our parishes to
join us as members. Keep us ever close to Our Lady, the Mother of Jesus,
so that through her intercession, your Son will strengthen our ideals of
charity, unity, fraternity, and patriotism and unite us in a common
bond, always in the service of your divine Son, Jesus Christ, who lives
and reigns now and forever, Amen.

~ Vivat Jesus! Aaron
