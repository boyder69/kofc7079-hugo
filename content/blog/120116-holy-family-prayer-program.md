+++
title = "Holy Family Prayer Program"
date = "2016-12-01"
tags = ["prayer"]
categories = ["events","Marian"]
banner = "img/posts/holy-family-prayer-card150.jpg"
author = "Andrew Boyd"
+++

## Join us for an evening of prayer on December 7th at OLBH.
<img style="float: right;" src="/img/posts/holy-family-prayer-card150.jpg">  

The Holy Family Prayer Program is the 17th Mariam Prayer Program sponsored by the Knights of Columbus, beginning with the first program honoring Mary under her title of Our Lady of Guadalupe in 1979. These Order wide programs of Marian Prayer have gathered more than 16 million people in prayer servers all over the world. This years program began August 4th at the 133rd Knights of Columbus Supreme Convention.  

The prayer service consists of an Opening Hymnn, Greeting, Opening Prayer, Liturgy of the Word, Catechesis by St. John Paul II, Intercession, the Most Holy Rosary of the Bessed Virgin Mary, Cetechesis of Pope Benedict XVI, Litany of the Holy Family, Catechesis of Pope Francis, Prayer for the Holy Family, Pope Francis, and final prayer. The Services will last about 1 hour and everyone is invited to stay for Eucharistic Benediction and Resposition after.
