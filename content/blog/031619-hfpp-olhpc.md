+++
title = "MARIAN PRAYER - OUR LADY HELP OF PERSECUTED CHRISTIANS"
date = "2019-03-24"
tags = ["prayer"]
categories = ["Marian"]
banner = "img/posts/olhpc1.PNG"
author = "Andrew Boyd"
+++

## View the Our Lady Help of Persecuted Christians at OLBH.
<img style="float: right;" style="margin:50" src="/img/posts/olhpc1.PNG">

The 2018-2019 Marian Prayer Program presents the 18th Marian image sponsored by the Knights of Columbus, this time of Our Lady Help of Persecuted Christians. Each Knights of Columbus jurisdiction receives several Marian images, which serve as the centerpieces for prayer services conducted in churches and council meeting places throughout the Order for the duration of the initiative. 
\
\
\
\
\
\
\
<img style="float: left;" style="margin:50" src="/img/posts/olhpc2.PNG">  
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
Since its inception in 1979, the Knights of Columbus Marian Prayer Program has held more than 166,900 local council and parish prayer services with some 20 million participants. This year, the prayer service is intended to raise awareness of the plight of Christians persecuted for their faith and to stand in prayerful solidarity with them.  
\
\
\
\
<img style="float: right;" style="margin:50" width="400" height="500" src="/img/posts/olhpc-frdd.jpg">  
\
\
\
Fr. Andrzej and District Deputy Philip Thompson on March 23rd at OLBH during the Stewardship Fair.  
\
\
Please take time to enjoy the Marian image while it is in our Parish and pray for persecuted Christians.  
