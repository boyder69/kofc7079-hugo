+++
title = "October - Month of the Rosary"
date = "2018-09-28"
tags = ["prayer"]
categories = ["events"]
banner = "img/posts/htptr.PNG"
author = "Andrew Boyd"
+++

## Come honor our mother and pray with us!
<img style="float: right;" src="/img/posts/htptr.PNG">  

Rosary's will be prayed Before the 5PM Saturday mass and the 8AM Sunday Mass, and following the11 AM Mass - every weekend in October. Join us in prayer to honor Our Mother Mary!   
